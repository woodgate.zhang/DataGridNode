#!/bin/bash
if [ ! -n "$1" ] ;then
    module=dataGnode
else
    module=$1
fi
if [ ! -n "$2" ] ;then
    network=test
else
    network=$2
fi
echo $network
echo "starting module - $module ..."
# stop all node app
echo "pm2 stop pm2-$module.json"
pm2 stop pm2-$module.json
echo "rm -rf log/$module"
rm -rf log/$module
echo "mkdir log/$module"
mkdir log/$module

ps -fe|grep ipfs |grep -v grep
if [ $? -ne 0 ]
then
echo "start ipfs daemon....."
nohup ipfs daemon  > ipfs.out 2>&1 &
else
echo "ipfs daemon is runing....."
fi

# start all node apps in development mode
echo "pm2 start pm2-$module.json --env $network"
pm2 start pm2-$module.json --env $network
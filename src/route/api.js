'use strict';

import { Router } from 'restify-router';
import { gridService } from '../service/GridService';
import { ipfsService } from "../core/service/IPFSService";
import { assistantService as assistant } from "../service/AssistantService";

const Debug = require('debug')('DataGrid:node:route:api');

export const router = new Router();

router.get('/hello', function (req, res, next) {
    assistant.sayHello(req, res, next);
});

router.get('/fs/cat/:hashId', function (req, res, next) {
    ipfsService.catFile(req, res, next);
});

router.get('/fs/get/:hashId', function (req, res, next) {
    ipfsService.getFile(req, res, next);
});

router.get('/fs/stat/:hashId', function (req, res, next) {
    ipfsService.statObject(req, res, next);
});

router.post('/add/data', function (req, res, next) {
    gridService.addNewResourceByData(req, res, next);
});

router.post('/add/file', function (req, res, next) {
    gridService.addNewResourceByFile(req, res, next);
});

router.post('/update/:hashId', function (req, res, next) {
    gridService.updateMetadataOfAsset(req, res, next);
});

router.get('/mine', function (req, res, next) {
    gridService.getMyResources(req, res, next);
});

'use strict';

require("babel-core/register");
require("babel-polyfill");

import fs from 'fs';
import url from 'url';
import empty from "is-empty";
import ecc from "eosjs-ecc";
import { Validator } from 'jsonschema';
import jsonSize from 'json-size';
import Config from '../../config/config';
import { UnknownEOSAccountError, EOSNetworkConnectionError } from "../core/other/CustomErrors";
import { FilteredMetadata } from '../core/other/FilteredObject';
import { ipfsClient as ipfs } from '../core/ipfs/IPFSClient';
import { EOSUtil, eosUtil } from '../core/chain/eos-util';
import { ETHUtil, ethUtil } from "../core/chain/eth-util";

const Debug = require('debug')('DataGrid:node:service:file');

const actionOptions = {
    authorization: 'mingqi@active',
    broadcast: true,
    sign: true
};

const jsonValidator = new Validator();

const ipfsOptionSchema = {
    "id": "/ipfsOptionSchema",
    "type": "object",
    "properties": {
        "recursive": {"type": "boolean"},
        "onlyHash": {"type": "boolean"},
        "pin": {"type": "boolean"}
    }
};
const signedExtraDataSchema = {
    "id": "/SignedExtraData",
    "type": "object",
    "properties": {
        "metadata"  : {"type": "string"},
        "signature" : {"type": "string"}
    },
    required: ['metadata', 'signature']
};
const signedExtraDataWithSourceSchema = {
    "id": "/SignedExtraDataWithSource",
    "type": "object",
    "properties": {
        "metadata"  : {"type": "object"},
        "account"   : {"type": "string"},
        "signature" : {"type": "string"},
        "source"    : {"type": "string"},
        "options"   : {
            "type": "object",
            "properties": {
                "recursive" : {"type": "boolean"},
                "onlyHash"  : {"type": "boolean"},
                "pin"       : {"type": "boolean"}
            }
        }
    },
    required: ['metadata', 'signature', 'source', 'account']
};
const signedMetadataForUpdateSchema = {
    "id": "/signedMetadataForUpdate",
    "type": "object",
    "properties": {
        "metadata"  : {"type": "object"},
        "account"   : {"type": "string"},
        "signature" : {"type": "string"},
        "options"   : {
            "type": "object",
            "properties": {
                "recursive" : {"type": "boolean"},
                "onlyHash"  : {"type": "boolean"},
                "pin"       : {"type": "boolean"}
            }
        }
    },
    required: ['metadata', 'signature', 'account']
};
const sortFields = ['_id', 'size', 'timestamp'];
const sortOrders = [-1, 1];
const chains = ['eos', 'ether'];

const maxSizeOfMetadataByBytes = Config.asset.metadata.sizeLimit.max * 1024;

class GridService {
    constructor() {
        this.ipfs = ipfs;
    }
    resetConnect(multiAddr) {
        this.ipfs.resetWithMultiAddr(multiAddr);
    };
    pUpdateMetadataOfAsset(hashId_source, metadata, options, eosAccount) {
        Debug("pUpdateMetadataOfAsset>");
        Debug("hashId_source: ", hashId_source);
        Debug("metadata: ", metadata);
        Debug("options: ", options);
        Debug("eosAccount: ", eosAccount);
        return new Promise((resolve, reject) => {
            let asset = {};
            asset.source = hashId_source;
            this.ipfs.addFile(metadata, options).then(files => {
                Debug(files);
                asset.metadata = files[0].hash;
                return eosUtil.eos.contract("aaa111");
            }).then(contract => {
                return contract.modify(eosAccount, asset.source, asset.metadata, actionOptions);
            }).then(action_ret => {
                Debug(action_ret);
                asset.tx = action_ret.transaction_id;
                resolve(asset);
            }).catch(error => {
                Debug("updateMetadataOfAsset> %s", error.toString());
                reject(error);
            })
        });

    }
    pAddSourcePlusMetadata(source, metadata, options, eosAccount) {
        return new Promise((resolve, reject) => {
            const promises = [
                new Promise((resolve, reject) => {
                    this.ipfs.addFile(source, options).then(files => {
                        Debug(files);
                        resolve({source: files[0].hash});
                    }).catch(error => {
                        reject(error);
                    })
                }),
                new Promise((resolve, reject) => {
                    this.ipfs.addFile(metadata, options).then(files => {
                        Debug(files);
                        resolve({metadata: files[0].hash});
                    }).catch(error => {
                        reject(error);
                    })
                })
            ];
            const asset = {};
            Promise.all(promises).then(ipfsItems => {
                for (let i = 0; i < ipfsItems.length; i++) {
                    Object.entries(ipfsItems[i]).forEach(([key, value]) => {
                        asset[key] = value;
                    });
                }
                return eosUtil.eos.contract("aaa111");
            }).then(contract => {
                return contract.add(eosAccount, asset.source, asset.metadata, actionOptions);
            }).then(action_ret => {
                Debug(action_ret);
                asset.tx = action_ret.transaction_id;
                resolve(asset);
            }).catch(error => {
                Debug("addSourcePlusMetadata> %s", error.toString());
                reject(error);
            });
        });
    };
    addNewResourceByFile(req, res, next) {
        Debug("addNewResourceByFile>");
        let jsonBody = req.body;
        const bodyType = typeof req.body;
        Debug("bodyType: ", bodyType);
        if (bodyType === 'string') {
            jsonBody = JSON.parse(req.body);
        }
        jsonBody.options = empty(jsonBody.options) ? {} : JSON.parse(jsonBody.options);
        Debug(jsonBody);
        const uploaded_file = req.files.file;  // File in form
        if(empty(uploaded_file)) {
            res.send(400, { error: 'no file detected' });
            return next();
        }
        const args = url.parse(req.url, true).query;
        Debug(args);
        const isValidOptionSchema = jsonValidator.validate(jsonBody.options, ipfsOptionSchema);
        if(!isValidOptionSchema.valid) {
            res.send(400, { error: 'invalid options' });
            return next();
        }
        const bufferedFile = fs.readFileSync(uploaded_file.path);
        if(empty(jsonBody)) { // no extra metadata is provided, just add file to ipfs
            this.ipfs.addFile(bufferedFile, jsonBody.options).then(files => {
                Debug(files);
                res.send(200, files);
                return next();
            }).catch(error => {
                res.send(500, { error: error.message });
                return next();
            });
        } else { // extra metadata is provided, add file to ipfs and metadata to mongodb
            // validate body parameters against required json schema
            const isValidSchema = jsonValidator.validate(jsonBody, signedExtraDataSchema);
            if(isValidSchema.valid === false){
                res.send(400, { error: 'invalid parameters' });
                return next();
            }
            Debug("metadata size: %d", jsonSize(JSON.parse(jsonBody.metadata)));
            if(jsonSize(JSON.parse(jsonBody.metadata)) > maxSizeOfMetadataByBytes) { // check metadata size exceed max or not
                res.send(400, { error: 'metadata size exceeded max size - ' + maxSizeOfMetadataByBytes + ' bytes' });
                return next();
            }
            try {
                const publicKey = ecc.recover(jsonBody.signature, jsonBody.metadata); // get public key
                eosUtil.getAccount(jsonBody.account).then(eosAccount => {
                    if(eosAccount.permissions[0].required_auth.keys[0].key !== publicKey) {
                        res.send(400, { error: 'account name mismatch with public key' });
                        return next();
                    }
                    return this.pAddSourcePlusMetadata(
                        bufferedFile,
                        Buffer(jsonBody.metadata),
                        jsonBody.options,
                        jsonBody.account);
                }).then(resource => {
                    res.send(200, resource);
                    return next();
                }).catch(error => {
                    res.send(500, { error: error.message });
                    return next();
                });
            } catch(error) {
                if( error instanceof SyntaxError) {
                    res.send(400, { error: 'value must be a string with valid json schema' });
                    return next();
                }
                res.send(400, { error: 'wrong data signature' });
                return next();
            }
        }
    };
    addNewResourceByData(req, res, next) {
        Debug('addNewResourceByData>');
        let jsonBody = req.body;
        const bodyType = typeof req.body;
        Debug("bodyType: ", bodyType);
        if (bodyType === 'string') {
            jsonBody = JSON.parse(req.body);
        }
        Debug(jsonBody);
        const isValidSchema = jsonValidator.validate(jsonBody, signedExtraDataWithSourceSchema);
        if(!isValidSchema.valid || empty(jsonBody.source)) {
            res.send(400, { error: 'invalid parameters' });
            return next();
        }
        jsonBody.options = empty(jsonBody.options) ? {} : jsonBody.options;
        Debug("basic schema is ok");
        Debug("options schema is ok");
        const args = url.parse(req.url, true).query;
        Debug(args);
        Debug("metadata size: %d", jsonSize(jsonBody.metadata));
        if(jsonSize(jsonBody.metadata) > maxSizeOfMetadataByBytes) { // check metadata size exceed max or not
            res.send(400, { error: 'metadata size exceeded max size - ' + maxSizeOfMetadataByBytes + ' bytes' });
            return next();
        }
        try {
            jsonBody.publicKey = ecc.recover(jsonBody.signature, JSON.stringify(jsonBody.metadata)); // get public key
            Debug(jsonBody);
            eosUtil.getAccount(jsonBody.account).then(eosAccount => {
                if(eosAccount.permissions[0].required_auth.keys[0].key !== jsonBody.publicKey) {
                    res.send(400, { error: 'account name mismatch with public key' });
                    return next();
                }
                return this.pAddSourcePlusMetadata(
                    Buffer(jsonBody.source),
                    Buffer(JSON.stringify(jsonBody.metadata)),
                    jsonBody.options,
                    jsonBody.account);
            }).then(resource => {
                res.send(200, resource);
                return next();
            }).catch(error => {
                res.send(500, { error: error.message });
                return next();
            });
        } catch(error) {
            if(error instanceof SyntaxError) {
                res.send(400, { error: 'value must be a string with valid json schema' });
                return next();
            }
            if(error instanceof UnknownEOSAccountError) {
                res.send(400, { error: 'unknown eos account name' });
                return next();
            }
            if(error instanceof EOSNetworkConnectionError) {
                res.send(400, { error: error.message });
                return next();
            }
            res.send(400, { error: 'wrong data signature' });
            return next();
        }
    };
    updateMetadataOfAsset(req, res, next) {
        Debug('updateMetadataOfAsset>');
        const hashId_source = req.params.hashId;
        Debug("hashId: %s", req.params.hashId);
        if( empty(req.params.hashId) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        let jsonBody = req.body;
        const bodyType = typeof req.body;
        Debug("bodyType: ", bodyType);
        if (bodyType === 'string') {
            jsonBody = JSON.parse(req.body);
        }
        Debug(jsonBody);
        const isValidSchema = jsonValidator.validate(jsonBody, signedMetadataForUpdateSchema);
        if(!isValidSchema.valid || empty(jsonBody.metadata)) {
            res.send(400, { error: 'invalid parameters' });
            return next();
        }
        jsonBody.options = empty(jsonBody.options) ? {} : jsonBody.options;
        Debug("basic schema is ok");
        Debug("options schema is ok");
        try {
            jsonBody.publicKey = ecc.recover(jsonBody.signature, JSON.stringify(jsonBody.metadata)); // get public key
            Debug(jsonBody);
            eosUtil.getAccount(jsonBody.account).then(eosAccount => {
                if(eosAccount.permissions[0].required_auth.keys[0].key !== jsonBody.publicKey) {
                    res.send(400, { error: 'account name mismatch with public key' });
                    return next();
                }
                return this.pUpdateMetadataOfAsset(
                    hashId_source,
                    Buffer(JSON.stringify(jsonBody.metadata)),
                    jsonBody.options,
                    jsonBody.account);
            }).then(asset => {
                res.send(200, asset);
                return next();
            }).catch(error => {
                res.send(500, { error: error.message });
                return next();
            });
        } catch(error) {
            if(error instanceof SyntaxError) {
                res.send(400, { error: 'value must be a string with valid json schema' });
                return next();
            }
            if(error instanceof UnknownEOSAccountError) {
                res.send(400, { error: 'unknown eos account name' });
                return next();
            }
            if(error instanceof EOSNetworkConnectionError) {
                res.send(400, { error: error.message });
                return next();
            }
            res.send(400, { error: 'wrong data signature' });
            return next();
        }
    };
    getMyResources(req, res, next) {
        Debug("getMyResources >");
        Debug("request.headers: %s", JSON.stringify(req.headers));

        let page = 1;
        let pageSize = 15;
        let sortField = sortFields[0];
        let order = -1;
        let chain = chains[0];

        const args = url.parse(req.url, true).query;
        Debug(args);

        if (args) {
            if (args.chain) {
                args.chain = args.chain.toLowerCase();
                const indexOfchain= chains.indexOf(args.chain);
                if(indexOfchain !== -1) { // standard chain fields: ['eos', 'ether']
                    chain = args.chain;
                }
            }
            if (args.sortBy) {
                sortField = args.sortBy;
                const indexOfsortField = sortFields.indexOf(sortField);
                if(indexOfsortField === -1) { // not standard query fields: ['_id', 'size', 'timestamp']
                    if(!sortField.startsWith('extra.')) { // not standard fields, not extra fields ether
                        res.send(400, 'wrong query fields');
                        return next();
                    }
                }
            }
            if (args.order) {
                if(!isNaN(Number(args.order))) {
                    const indexOfOrder = sortOrders.indexOf(Number(args.order));
                    if(indexOfOrder !== -1) {
                        order = sortOrders[indexOfOrder];
                    }
                }
            }
            if(args.page) {
                if(!isNaN(Number(args.page))) {
                    page = Number(args.page);
                }
            }
            if(args.pageSize) {
                if(!isNaN(Number(args.pageSize))) {
                    pageSize = Number(args.pageSize);
                    pageSize = pageSize >= 50 ? 50 : pageSize;
                }
            }
        }
        if( empty(req.headers.signature) || empty(req.headers.message) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        try {
            let publicKey = '';
            if(chain === 'eos') {
                publicKey = ecc.recover(req.headers.signature, req.headers.message); // get public key
            } else if(chain === 'ether') {
                publicKey = ETHUtil.recover(req.headers.signature, ethUtil.signedSha3(req.headers.message)); // get public key
            }
            Debug("messageHash: " + ethUtil.signedSha3(req.headers.message));
            Debug("pub key: " + publicKey);
            MetadataService.find({ publicKey: publicKey }, page, pageSize, sortField, order).then(result => {
                let filteredMetadatas = [];
                for (let i = 0; i < result.docs.length; i++) {
                    filteredMetadatas.push(new FilteredMetadata(result.docs[i]));
                }
                result.docs = filteredMetadatas;
                console.log(result);
                res.send(200, result);
                return next();
            }).catch(error => {
                res.send(500, error.message);
                return next();
            })
        } catch(error) {
            console.log(error);
            res.send(400, new Error('invalid signature'));
            return next();
        }
    }
    getMyDashboard(req, res, next) {
        Debug("getMyDashboard >");
        Debug("request.headers: %s", JSON.stringify(req.headers));
        let chain = chains[0];
        const args = url.parse(req.url, true).query;
        Debug(args);

        if (args) {
            if (args.chain) {
                args.chain = args.chain.toLowerCase();
                const indexOfchain= chains.indexOf(args.chain);
                if(indexOfchain !== -1) { // standard chain fields: ['eos', 'ether']
                    chain = args.chain;
                }
            }
        }
        if( empty(req.headers.signature) || empty(req.headers.message) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        try {
            let publicKey = '';
            if(chain === 'eos') {
                publicKey = ecc.recover(req.headers.signature, req.headers.message); // get public key
            } else if(chain === 'ether') {
                publicKey = ETHUtil.recover(req.headers.signature, ethUtil.signedSha3(req.headers.message)); // get public key
            }
            Metadata.aggregate([
                {
                    $match: {
                        publicKey: publicKey
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalSize: { $sum: "$size" },
                        totalCount: { $sum: 1}
                    }
                }
            ]).then(result => {
                console.log(result);
                res.send(200, result);
            }).catch(error => {
                res.send(500, error.message);
                return next();
            });
        } catch(error) {
            console.log(error);
            res.send(400, new Error('invalid signature'));
            return next();
        }
    }
}

const gridService = new GridService(ipfs);

export  {
    gridService
};
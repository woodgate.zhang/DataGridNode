'use strict';

require("babel-core/register");
require("babel-polyfill");

import ecc from "eosjs-ecc";
import Config from '../../config/config';
import { ipfsClient as ipfs } from '../core/ipfs/IPFSClient';

const Debug = require('debug')('DataGrid:node:service:assistant');

const MetadataProducerPublicKey = ecc.privateToPublic(Config.MetadataProducer.privateKey);

class NodeAssistant {
    constructor() {
        this.ipfs = ipfs;
    }
    sayHello(req, res, next) {
        try {
            let nodeInfo = {};
            //const userPublicKey = ecc.recover(req.headers.signature, req.headers.msg); // get public key
            nodeInfo.mpPubKey = MetadataProducerPublicKey;
            this.ipfs.getVersion().then(version => {
                nodeInfo.ipfsVersion = version;
                res.send(200, nodeInfo);
                return next();
            }).catch(error => {
                Debug('ipfs.getVersion > %s', error.message);
                nodeInfo.ipfsVersion = {};
                res.send(200, nodeInfo);
                return next();
            });
        } catch(error) {
            console.log(error);
            res.send(400, new Error('invalid signature'));
            return next();
        }
    }
}

const assistantService = new NodeAssistant(ipfs);

export  {
    assistantService
};

import fs from 'fs';
import path from 'path';
import jsonSize from 'json-size';
import Config from '../../config/config';

const max = Config.asset.metadata.sizeLimit.max;


const bufferedFile = fs.readFileSync(path.resolve(__dirname, '../../resource/json_metadata.txt'));
console.log(bufferedFile.byteLength);
console.log(bufferedFile.toString('utf8').length);

console.log(jsonSize(JSON.parse(bufferedFile.toString('utf8'))));
console.log(max * 1024);
'use strict';

import chai from 'chai';
import mongoose from "mongoose";
import Config from '../../config/config';
import { MongoService } from '../core/service/MongoService';

const expect = chai.expect;
const should = chai.should();
const mdb_url = "mongodb://" + Config.mongo.host + ":" + Config.mongo.port + "/test";

describe('.mongo (the Mongodb API part)', function () {
    //this.timeout(120 * 1000);
    before((done) => {
        mongoose.connect(mdb_url, { useNewUrlParser: true }).then(() => {
            done();
        }).catch(error => {
            should.not.exist(error);
        });
    });
    after((done) => {
        console.log('drop db');
        mongoose.connection.dropDatabase();
        done();
    });
    it('mongo.add', function (done) {
        const jsonData = {
            "block": 13984385,
            "thxId": "e25a56c5d45a6f7eb081299c7f628722696846fab7e23e6ba09f6e0f69a4877e",
            "account": "mingqi",
            "hashId": "QmUHhQrabcfeJqmegn8DBWyR5qBHtmgi3a2DTfjctVDtiZ",
            "extra": {
                "name": "test.txt",
                "desc": "spaceX third launch",
                "location": "Kennedey"
            },
            "timestamp": "2018-09-11T23:37:19.500Z",
            "size": 70
        };
        MongoService.deleteThenAddMetadataTHX(jsonData).then(data => {
            expect(data.hashId).to.equal(jsonData.hashId);
            data.should.have.property('_id');
            done();
        }).catch(error => {
            return done(error);
        });
    });
    it('mongo.find', function (done) {
        MongoService.find({ hashId: 'QmUHhQrabcfeJqmegn8DBWyR5qBHtmgi3a2DTfjctVDtiZ' }, 1, 10)
        .then((result) => {
            result.should.have.property('docs');
            result.total.should.equal(1);
            result.page.should.equal(1);
            result.limit.should.equal(10);
            done();
        }).catch(error => {
            console.log(error);
            return done(error)
        });
    });
});

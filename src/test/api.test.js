'use strict';

require('dotenv').config();

import fs from 'fs';
import path from 'path';
import chai from 'chai';
import request from 'request';
import ecc from 'eosjs-ecc';
import jsonSize from 'json-size';
import { eosUtil } from '../core/chain/eos-util';

const expect = chai.expect;
const should = chai.should();
const ethPrivateKey = '0x04f60ce3d41707bf8907abff5e962c09a8bf4e35a571f4ee51984aace96c27f0';
const eosPrivateKey = '5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHKK';
const publicKey = ecc.privateToPublic(eosPrivateKey);


console.log("env.type: ", process.env.TYPE);
let sosnode_port = 3002;   // default to test port
if(process.env.TYPE === '1') { // product environment
    sosnode_port = 9527;
}
console.log("port: ", sosnode_port);

let host = '47.99.61.209';
if(process.env.NETWORK === 'local') { // local environment
    host ="127.0.0.1";
}
console.log("host: ", host);

describe('.asset operation (files/data)', function () {
    this.timeout(120 * 1000);
    before((done) => {
        console.log("starting executing testcases...");
        done();
    });
    after((done) => {
        console.log("all testcases are done!");
        done();
    });
    it('api.asset.file.add', function (done) {
        fs.writeFileSync(path.resolve(__dirname, '../../resource/test-file.txt'), new Date() + Math.random().toString(36).substring(8));
        const metadata = '{"name": "test.txt", "desc":"spaceX third launch", "location": "Kennedey"}';
        const readFileStream = fs.createReadStream(path.resolve(__dirname, '../../resource/test-file.txt'));
        const formData = {
            account: 'mingqi',
            metadata: metadata,
            signature: ecc.sign(metadata, eosPrivateKey),
            options: '{ "onlyHash": false }',
            file: readFileStream
        };
        request.post({ url: 'http://' + host +':' + sosnode_port + '/add/file' , formData: formData }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return done(err);
            }
            console.log("server response: ", body);
            body = JSON.parse(body);
            body.should.have.property('source');
            body.should.have.property('metadata');
            done();
        });
    });
    it('api.asset.file.add: metadata exceeds maxSize', function (done) {
        fs.writeFileSync(path.resolve(__dirname, '../../resource/test-file.txt'), new Date() + Math.random().toString(36).substring(8));
        const bufferedMetadata = fs.readFileSync(path.resolve(__dirname, '../../resource/json_metadata.txt'));
        const metadata = bufferedMetadata.toString();
        console.log("metadata size: ", metadata.length);
        const readFileStream = fs.createReadStream(path.resolve(__dirname, '../../resource/test-file.txt'));
        const formData = {
            account: 'mingqi',
            metadata: metadata,
            signature: ecc.sign(metadata, eosPrivateKey),
            options: '{ "onlyHash": false }',
            file: readFileStream
        };
        request.post({ url: 'http://' + host +':' + sosnode_port + '/add/file' , formData: formData }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return done(err);
            }
            console.log("server response: ", body);
            body = JSON.parse(body);
            body.should.have.property('error');
            done();
        });
    });
    it('api.asset.data.add', function (done) {
        const strSource = '{"name":"wangpengpeng","gender":"male","age":28}' + Date.now() + Math.random().toString(36).substring(7);
        const metadata = {"name": "test.txt", "desc":"spaceX third launch", "location": "Kennedey"};
        const bodyParams = {
            "account": "mingqi",
            "metadata": metadata,
            "signature": ecc.sign(JSON.stringify(metadata), eosPrivateKey),
            "source": strSource,
            "options": { onlyHash: false }
        };
        console.log(JSON.stringify(bodyParams));
        request.post({
            url: 'http://' + host +':'  + sosnode_port + '/add/data/',
            json: true,
            body: bodyParams }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return done(err);
            }
            console.log("server response: ", body);
            body.should.have.property('source');
            body.should.have.property('metadata');
            done();
        });
    });
    it('api.asset.data.add: metadata exceeds maxSize', function (done) {
        const strSource = '{"name":"wangpengpeng","gender":"male","age":28}' + Date.now() + Math.random().toString(36).substring(7);
        //const metadata = {"name": "test.txt", "desc":"spaceX third launch", "location": "Kennedey"};
        const bufferedMetadata = fs.readFileSync(path.resolve(__dirname, '../../resource/json_metadata.txt'));
        const metadata = JSON.parse(bufferedMetadata.toString());
        console.log(jsonSize(metadata));
        const bodyParams = {
            "account": "mingqi",
            "metadata": metadata,
            "signature": ecc.sign(JSON.stringify(metadata), eosPrivateKey),
            "source": strSource,
            "options": { onlyHash: false }
        };
        request.post({
            url: 'http://' + host +':'  + sosnode_port + '/add/data/',
            json: true,
            body: bodyParams }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return done(err);
            }
            console.log("server response: ", body);
            body.should.have.property('error');
            done();
        });
    });
    it('api.asset.update', function (done) {
        eosUtil.getTableRows("aaa111", "mingqi", "records").then(res => {
            //console.log(res.rows);
            if(res.rows.length > 0) {
                const sourceHashId = res.rows[0].hash_data;
                const metaHashId = res.rows[0].hash_meta;
                const metadata = {"name": "test.txt", "desc":"this is testing update", "location": "Kennedey"};
                const bodyParams = {
                    account: 'mingqi',
                    metadata: metadata,
                    signature: ecc.sign(JSON.stringify(metadata), eosPrivateKey),
                    options: { "onlyHash": false },
                };
                request.post({
                    url: 'http://' + host +':'  + sosnode_port + '/update/' + sourceHashId,
                    json: true,
                    body: bodyParams }, function optionalCallback(err, httpResponse, body) {
                    if (err) {
                        return done(err);
                    }
                    console.log("server response: ", body);
                    body.should.have.property('source');
                    body.should.have.property('metadata');
                    expect(body.metadata).to.not.equal(metaHashId);
                    done();
                });
            } else {
                console.error("no asset detected!");
                done();
            }
        });

    });
});
